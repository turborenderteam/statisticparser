# -*- coding:utf-8 -*-
"""
    Скрипт выполняет парсинг указанного каталога в поисках файлов статистики.
    Найденные файлы статистики сверяются с БД портала и биллинга, затем
    извлекаются дополнительные данные и все записывается в БД MongoDB.
    Автор: С.Томилов
    03.11.2015
"""

import sys
import os
import json
import psycopg2
import mysql.connector
import time
from pymongo import MongoClient
import xml.etree.ElementTree as ET
import shutil
import datetime


class ErrorTask(Exception):
    def __init__(self, task_id):
        super(ErrorTask, self).__init__('Error Task {}.'.format(task_id))
        self.task_id = task_id


class StoppedTask(Exception):
    """
    """
    def __init__(self, task_id):
        super(StoppedTask, self).__init__('Task {} has been stopped.'.format(task_id))
        self.task_id = task_id

class EmptyBilling(Exception):
    """ Исключение в биллинге не надена наработка для задачи (task_id)
    """
    def __init__(self, task_id):
        super(EmptyBilling, self).__init__('Billing for task={} does not exists.'.format(task_id))
        self.task_id = task_id


class BadFrames(Exception):
    def __init__(self, task_id, msg):
        super(BadFrames, self).__init__('Frames problem in Task {}: {}'.format(task_id, msg))
        self.task_id = task_id


class TaskDoesNotComplete(Exception):
    """ Исключение - задача еще не готова.
    """
    def __init__(self, task_id, status):
        super(TaskDoesNotComplete, self).__init__('Task {} does not Complete ({})'.format(task_id, status))
        self.task_id = task_id
        self.status = status


class PrerenderComplete(Exception):
    """ Исключение - задача является пререндером.
    """
    def __init__(self, task_id):
        super(PrerenderComplete, self).__init__('Task {} is prerender'.format(task_id))
        self.task_id = task_id


class ErrorFileFound(Exception):
    """ Исключение - найден файл ошибки сбора стистики.
    """
    def __init__(self, task_id):
        super(ErrorFileFound, self).__init__('Found ERROR file in statistic of task {}'.format(task_id))
        self.task_id = task_id


def connect_caeserver():
    cur = None
    try:
        conn = psycopg2.connect(host="caeserver", port=5432, user="postgres", password="1234", database="trunk")
        cur = conn.cursor()
    except psycopg2.Error as e:
        raise Exception("Error caeserver connection {}".format(e.message))
    return cur


def connect_billing():
    conn = None
    try:
        conn = mysql.connector.connect(host='10.5.0.3', user='root', password='root', database='bgbilling')
    except Exception as e:
        raise Exception("Error billing connection {}".format(e))
    return conn


def connect_mongodb():
    conn = None
    try:
        conn = MongoClient(host='10.5.0.1', port=27017)
    except Exception as e:
        raise Exception("Error MongoDB connection {}.".format(e))
    return conn


def caeserver_frames(cae_cur, task_id):
    """ Находит количество обработанных кадров по id задачи.
    :param cae_cur:
    :param task_id:
    :return frames_complete: количество отрендереных кадров, либо (-1) в случае ошибки.
    """
    frames_complete = -1
    status = None
    select = """SELECT frame_count, frame_finished_count, status FROM tbl_monitor_progress WHERE id='{}'""".format(task_id)
    #print("Execute: {}".format(select))
    try:
        cae_cur.execute(select)
        rows = cae_cur.fetchall()
        if rows:
            row = rows[0]

            frame_count = int(row[0])
            frame_finished_count = int(row[1])
            status = row[2]
            #print("{}: frame_count={}, frame_finished_count={}".format(status, frame_count, frame_finished_count))
            if status in ['Complete', 'Prerender complete']:
                if frame_count <= 0:
                    raise Exception("Frame count is 0")
                if frame_finished_count <= 0:
                    raise Exception("Frame finished count is 0")
                if frame_finished_count > frame_count:
                    raise Exception("Frame finished count > Frame count")
                frames_complete = frame_finished_count
    except Exception as e:
        raise BadFrames(task_id, e.message)
    return status, frames_complete


def caeserver_product_name(cae_cur, task_id):
    """ Находит название продукта по сцене.
    :param cae_cur:
    :param task_id:
    :return:
    """
    name = None
    try:
        select = """SELECT bean.name FROM tbl_caebean AS bean, tbl_caetask AS task
        WHERE task.id = '{task_id}' AND task.caebean = bean.id""".format(task_id=task_id)
        #print("Execute: {}".format(select))
        cae_cur.execute(select)
        rows = cae_cur.fetchall()
        if rows:
            name = rows[0][0]
    except Exception as e:
        raise Exception("ERROR in caeserver_product_name: {}".format(e))
    return name


def billing_tables(bil_cur):
    """ Находит список таблиц БД биллинга в которых может быть статистика по Ггц.
    :param bil_cur:
    :return:
    """
    bil_cur.execute("SHOW TABLES LIKE 'rscm_service_account_6_20%'")
    tables = []
    for t in bil_cur.fetchall():
        tables.append(t[0])
    return tables[::-1]


def billing_amounts(bil_cur, tables, task_id):
    """ Находит наработку для задачи.
    :param tables:
    :param task_id:
    :return amounts: list of int (Гц).
    """
    amounts = []
    try:
        for table in tables:
            select_billing = """SELECT amount FROM {table} WHERE comment LIKE '%{task_id}%'""".format(
                table=table, task_id=task_id)
            #print('Execute: {}'.format(select_billing))

            bil_cur.execute(select_billing)
            rows = bil_cur.fetchall()
            if rows:
                for r in rows:
                    amounts.append(int(r[0]))
            else:
                if amounts:
                    break
        if not amounts:
            raise EmptyBilling(task_id)
    except EmptyBilling as eb:
        raise
    except Exception as e:
        raise Exception('ERROR in billing_amounts: {}'.format(e))
    return amounts


def scene_type(task_id):
    """ Определяет тип задачи по ее id.
    :param task_id:
    :return:
    """
    scene_type_value = None
    try:
        full_path = r"P:\tasks\{}\Project.xml".format(task_id)
        if not os.path.isfile(full_path):
            raise Exception("File doesn't exists {}".format(full_path))
        with open(full_path) as project:
            tree = ET.parse(project)
            root = tree.getroot()
            scene_param = root.find('problemCaebean').find('categories').find('category[@name="sceneParam"]')
            if scene_param is not None:
                scene_type_value = scene_param.find('parameter[@name="scene_type"]').find('value').text
        if scene_type_value is None:
            raise Exception('Scene type not found')
    except Exception as e:
        raise Exception("ERROR in scene_type: {}".format(e))
    return scene_type_value


def is_distribute(scene):
    return scene == "scn_static_strip"


def is_static(scene):
    return "static" in scene


def determine_render(scene_data):
    if "Corona params" in scene_data:
        return "Corona"
    elif "VRay params" in scene_data:
        return "VRay"
    elif "Mental Ray params" in scene_data:
        return "MentalRay"
    elif "Default Scanline params" in scene_data:
        return "Scanline"
    elif "Maxwell params" in scene_data:
        return "Maxwell"
    elif "Quicksilver params" in scene_data:
        return "Quicksilver"
    else:
        return "Undefined"


def portal_add_params(task_id):
    """ Находит значения дополнительных параметров сцены, задаваемых на портале.
    :param task_id:
    :return:
    """
    add_width = None
    add_height = None
    try:
        full_path = r"P:\tasks\{}\cmd-executor.log".format(task_id)
        if not os.path.isfile(full_path):
            raise Exception("File doesn't exists {}".format(full_path))
        with open(full_path, 'r') as cmd_executor_log:
            for line in cmd_executor_log:
                if '-width' in line:
                    w = line.replace('-width=', '').replace('\n', '')
                    add_width = 0
                    if w: add_width = int(w)
                if '-height' in line:
                    h = line.replace('-height=', '').replace('\n', '')
                    add_height = 0
                    if h: add_height = int(h)
                if add_width is not None and add_height is not None:
                    break
        if add_width is None and add_height is None:
            raise Exception('-width or -height not found in file {}'.format(full_path))
    except Exception as e:
        raise Exception("ERROR in scene_type: {}".format(e))
    return add_width, add_height


def move_statistic(src, dst):
    """ Рекурсивно перемещает каталог из src в dst.
    :param src:
    :param dst:
    :return:
    """
    src = src.replace('\\', '/')
    dst = dst.replace('\\', '/')
    shutil.move(src, dst)
    print ("{} -> {}".format(src, dst))


def parse_statistic(path, task_id, cae_cur, bil_cur, mongo_db):
    """
    :param path:
    :type path: str
    :param task_id:
    :type task_id: str
    :param bil_cur:
    :type bil_cur:
    :param mongo_db:
    :type mongo_db:
    :return result: 0 - no processed; 1 - successful; -1 - move as error; -2 - move as prerender; -3 - stopped.
    :rtype:
    """
    result = 0

    new_dir_path = path + '\\new\\' + task_id
    success_dir_path = path + '\\success\\' + task_id
    error_dir_path = path + '\\error\\' + task_id
    prerender_dir_path = path + '\\prerender\\' + task_id
    stopped_dir_path = path + '\\stopped\\' + task_id

    print(new_dir_path)
    stat_file_path = new_dir_path + '\\' + task_id + '.json'
    error_file_path = new_dir_path + '\\ERROR'

    try:
        # Check date modify
        td = datetime.timedelta(hours=12)
        m_time = os.stat(new_dir_path).st_mtime
        m_ctime = time.ctime(m_time)
        t0 = datetime.datetime.fromtimestamp(m_time)
        if datetime.datetime.now() - t0 < td:
            print('Time')
            return 0
            #raise Exception("DateTime: {}".format(m_ctime))

        if os.path.isfile(error_file_path):
            raise ErrorFileFound(task_id)

        if not os.path.isfile(stat_file_path):
            raise Exception("File {} doesn't exists.".format(stat_file_path))

        # To processing
        status, frames_count = caeserver_frames(cae_cur, task_id)
        if status != 'Complete':
            if status in ['Prerender complete', 'Prerender running']:
                raise PrerenderComplete(task_id)

            if status in ['Stop', 'Error', 'Rendering']:
                td_2w = datetime.timedelta(weeks=2)
                if datetime.datetime.now() - t0 > td_2w:
                    if status == 'Stop':
                        raise StoppedTask(task_id)
                    elif status in ['Error', 'Rendering']:
                        raise ErrorTask(task_id)
            raise TaskDoesNotComplete(task_id, status)

        product_name = caeserver_product_name(cae_cur, task_id)
        scene_type_value = scene_type(task_id)
        distribute = is_distribute(scene_type_value)
        static = is_static(scene_type_value)

        add_width, add_height = portal_add_params(task_id)

        bil_tables = billing_tables(bil_cur)
        # Ггц, кол.чанков.
        amounts = billing_amounts(bil_cur, bil_tables, task_id)
        total_amount = 0
        for a in amounts:
            total_amount += a

        # for amount in amounts:
        #     new_net_job.add_chunk(Chunk(amount=amount))

        mongo_record = {}
        mongo_record['task_id'] = task_id
        mongo_record['product'] = product_name
        mongo_record['frames_count'] = frames_count
        mongo_record['distribute'] = distribute
        mongo_record['static'] = static
        mongo_record['amount'] = total_amount #new_net_job.amount()
        mongo_record['add_width'] = add_width
        mongo_record['add_height'] = add_height
        mongo_record['chunks'] = []
        for a in amounts:
            mongo_record['chunks'].append({'amount': a})

        # данные из файла статистики
        with open(stat_file_path) as stat_file:
            # replace TO real boolean
            scene_data = stat_file.read()
            scene_data = scene_data.replace("\"false\"", "false").replace("\"true\"", "true")

            json_data = json.loads(scene_data)
            mongo_record['scene_data'] = json_data
            mongo_record['renderer'] = determine_render(json_data)

        result = mongo_db['3dsmax'].insert_one(mongo_record)
        if result is None:
            raise Exception('Failed insert MongoDB')

        print(result.inserted_id)
        move_statistic(new_dir_path, success_dir_path)  # move to success dir
        result = 1

    except StoppedTask as ste:
        print(ste)
        move_statistic(new_dir_path, stopped_dir_path)  # move to stopped dir
        result = -3
    except EmptyBilling as ebe:
        print(ebe)
        with open(new_dir_path + '\\PARSING_ERROR', 'wt') as parsing_error_file:
            parsing_error_file.write(ebe.message)
        move_statistic(new_dir_path, error_dir_path)  # move to error dir
        result = -1
    except BadFrames as bfe:
        print(bfe)
        with open(new_dir_path + '\\PARSING_ERROR', 'wt') as parsing_error_file:
            parsing_error_file.write(bfe.message)
        move_statistic(new_dir_path, error_dir_path)  # move to error dir
        result = -1
    except ErrorFileFound as efe:
        print(efe)
        move_statistic(new_dir_path, error_dir_path)  # move to error dir
        result = -1
    except PrerenderComplete as pe:
        print(pe)
        move_statistic(new_dir_path, prerender_dir_path)  # move to prerender dir
        result = -2
    except TaskDoesNotComplete as e:
        print("Skip: {}".format(e))
        result = 0
    except Exception as e:
        raise

    return result


def parse_statistics(path, cae_cur, bil_cur, mongo_db):
    scan_path = path + '\\new'
    dirs = os.listdir(scan_path)
    total_add = 0
    total_err = 0  # moved ERROR count
    total_pre = 0
    total_stop = 0

    for d in dirs:
        task_id = str(d)
        try:
            result = parse_statistic(path, task_id, cae_cur, bil_cur, mongo_db)
        except Exception as e:
            print ("Error processing {}: {}".format(task_id, e))
            result = 0

        if result == 1:
            total_add += 1
        elif result == -1:
            total_err += 1
        elif result == -2:
            total_pre += 1
        elif result == -3:
            total_stop += 1

    print("\ttotal add: {}\n\ttotal err: {}\n\ttotal pre: {}\n\ttotal stopped: {}".format(
        total_add, total_err, total_pre, total_stop))


def main():
    cae_cur = None
    billing_conn = None
    bil_cur = None

    try:
        cae_cur = connect_caeserver()
        billing_conn = connect_billing()
        if billing_conn is not None:
            bil_cur = billing_conn.cursor()

        mongo_conn = connect_mongodb()
        mongo_db = mongo_conn.statistic

        statistics_path = None
        single_id = None

        if len(sys.argv) < 2:
            raise Exception("Need params")

        i = 1
        while i < len(sys.argv):
            if sys.argv[i] in ['-p', '--path']:
                i += 1
                statistics_path = sys.argv[i]
            elif sys.argv[i] in ['-i', '--id']:
                i += 1
                single_id = sys.argv[i]
            else:
                print('unknown argument {}, skip it.'.format(sys.argv[i]))
            i += 1

        if statistics_path is None:
            raise Exception(r'Need "-p <--path>" parameter.')

        # statistics_path = sys.argv[1]
        #print("Processing {}".format(statistics_path))
        if single_id is not None:
            parse_statistic(statistics_path, single_id, cae_cur, bil_cur, mongo_db)
        else:
            parse_statistics(statistics_path, cae_cur, bil_cur, mongo_db)
    except Exception as e:
        print("FATAL ERROR: {}".format(e))
    finally:
        if cae_cur is not None:
            pass

        if bil_cur is not None:
                bil_cur.close()
        if billing_conn is not None:
            billing_conn.close()


if __name__ == "__main__":
    main()