# -*- coding:utf-8 -*-
import io
import sys
import os


def rename_output(dir_path, d):
    src_file = dir_path + '\\Output.json'
    dst_file = dir_path + '\\' + d + '.json'
    if os.path.isfile(src_file):
        os.rename(src_file, dst_file)
        print('{} -> {}'.format(src_file, dst_file))


def fix_version(dir_path, d):
    """
    :param dir_path:
    :param d:
    :return:
    """
    try:
        file_path = dir_path + '\\' + d + '.json'
        err_file_path = dir_path + '\\ERROR'
        if os.path.isfile(err_file_path):
            print('Skip ERROR found!')
        elif not os.path.isfile(file_path):
            raise Exception("Statistic file doesn't exists.")
        else:
            src_file = open(file_path)
            contents = src_file.read()
            src_file.close()
            replaced_contents = contents.replace("\"STATISTIC SCRIPT VERSION\": 0.0.2,",
                                                 "\"STATISTIC SCRIPT VERSION\": \"0.0.2\",")
            tmp_file_name = file_path + ".0"
            tmp_file = open(tmp_file_name, 'w+')
            tmp_file.write(replaced_contents)
            tmp_file.close()
            os.remove(file_path)  # remove the original
            os.rename(tmp_file_name, file_path)
    except Exception as e:
        print('Error {}'.format(e))


def process(root_path):
    dirs = os.listdir(root_path)
    for d in dirs:
        dir_path = root_path + '\\' + d
        print(dir_path)
        rename_output(dir_path, d)
        #fix_version(dir_path, d)


def main():
    if len(sys.argv) < 1:
        print("No args, exit!")
        return -1
    process(sys.argv[1])


if __name__ == "__main__":
    main()