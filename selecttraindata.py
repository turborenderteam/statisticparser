# -*- coding:utf-8 -*-

import os,sys
from pymongo import MongoClient
from enum import Enum
from math import ceil
import csv
import random

AMOUNT_DEVIDER = 1000000000.0


class Function(Enum):
    # span: [-inf..+inf]
    LINEAR = 0
    # span: [0..1]
    SIGMOID = 1
    # span: [-1..+1]
    SIGN_SIGMOID = 2


def max_min_list(data):
    """ Формирует списки максимальных и минимальных значений для всех списка входящих данных данных.
    :param data:
    :return:
    """
    max_list = [sys.float_info.min] * len(data[0])
    min_list = [sys.float_info.max] * len(data[0])

    for values in data:
        for j, value in enumerate(values):
            if value > max_list[j]:
                max_list[j] = value
            if value < min_list[j]:
                min_list[j] = value
    return max_list, min_list


def average_list(data):
    avr_list = [0.0] * len(data[0])

    for values in data:
        for j, value in enumerate(values):
            avr_list[j] += value

    num_samples = len(data)
    for i in range(0, len(avr_list)):
        avr_list[i] /= num_samples
    return avr_list


class PrettyFloat(float):
    def __repr__(self):
        return "%0.10f" % self


def normalize_data(data, function, exclude_indexes=[]):
    """ Выполняет нормализацию данных (data) под диапазон примлемый для выбранной функции (function).
    Элементы с индексами (exclude_indexes) - исключаются из нормализации.
    :param data:
    :param function:
    :param exclude_indexes:
    :return:
    """
    num_values = len(data[0])

    max_list, min_list = max_min_list(data)
    avr_list = average_list(data)
    print ('Max: {}'.format(max_list))
    print ('Min: {}'.format(min_list))
    print ('Avr: {}'.format(avr_list))

    abs_list = [0.0] * num_values
    norm_list = [0.0] * num_values
    trans_list = [0.0] * num_values

    for i, (ma, mi) in enumerate(zip(max_list, min_list)):
        abs_list[i] = (ma - mi)
        trans_list[i] = abs_list[i] - max_list[i]
        norm_list[i] = 1.0/abs_list[i]
    print ('Trans: {}'.format(trans_list))
    print ('Norm: {}'.format(map(PrettyFloat, norm_list)))
    #
    for s, sample in enumerate(data):
        for v, value in enumerate(sample):
            if function == Function.LINEAR:
                data[s][v] = value
            elif function == Function.SIGMOID:
                if v not in exclude_indexes:
                    data[s][v] += trans_list[v]
                    data[s][v] *= norm_list[v] #1.0/abs_list[v]
            elif function == Function.SIGN_SIGMOID:
                if v not in exclude_indexes:
                    data[s][v] -= avr_list[v]
                    data[s][v] /= max_list[v]
            else:
                pass

    with open('scale.txt', 'wt') as scale_file:
        global AMOUNT_DEVIDER
        scale_file.write('{0:.10f}\n'.format(AMOUNT_DEVIDER))
        scale_file.write('{0:.10f}\n'.format(norm_list[0]))
        scale_file.write('{0:.10f}\n'.format(trans_list[0]))

        scale_file.write('($VAL/{:.10f}-({})) * {}'.format(norm_list[0], trans_list[0], AMOUNT_DEVIDER))

    return data


def renderer(renderer_name):
    renderers = {'Undefined': 0.0,
                 'Corona': 1.0,
                 'VRay': 2.0,
                 'MentalRay': 3.0,
                 'Scanline': 4.0,
                 'Maxwell': 5.0,
                 'Quicksilver': 6.0
                 }
    return renderers[renderer_name]


def select_values(doc, function):
    task_id = doc['task_id']
    sd = doc['scene_data']

    amount = float(doc['amount'])
    frames_count = float(doc['frames_count'])
    amount_per_frame = amount / frames_count

    global AMOUNT_DEVIDER
    values = [amount_per_frame/AMOUNT_DEVIDER]  # 0

    if function in [Function.LINEAR, Function.SIGMOID]:
        values.append((0.0, 1.0)[doc['distribute']])  # 1
        values.append((0.0, 1.0)[doc['static']])  # 2
        values.append((0.0, 1.0)[sd['Hidden Geometry']])  # 3
        values.append((0.0, 1.0)[sd['Area Lights']])  # 4
    elif function == Function.SIGN_SIGMOID:
        values.append((-1.0, 1.0)[doc['distribute']])  # 1
        values.append((-1.0, 1.0)[doc['static']])  # 2
        values.append((-1.0, 1.0)[sd['Hidden Geometry']])  # 3
        values.append((-1.0, 1.0)[sd['Area Lights']])  # 4
    else:
        values.append(float(doc['distribute']))  # 1
        values.append(float(doc['static']))  # 2
        values.append(float(sd['Hidden Geometry']))  # 3
        values.append(float(sd['Area Lights']))  # 4

    #values.append(float(doc['frames_count']))
    #values.append(float(len(doc['chunks'])))

    # W,H
    width = sd['Width']
    height = sd['Height']

    if doc['add_width'] != 0:
        width = doc['add_width']
    if doc['add_height'] != 0:
        height = doc['add_height']
    if doc['renderer'] == 'VRay':
        vr = sd['VRay params']
        if not vr['output_getsetsfrommax']:
            width = vr['output_width']
            height = vr['output_height']

    values.append(float(width))
    values.append(float(height))
    resolution = width * height
    values.append(float(resolution))

    #values.append(renderer(doc['renderer']))

    values.append(float(sd['Verts']))
    values.append(float(sd['Faces']))
    values.append(float(sd['Poly']))
    values.append(float(sd['Scene size']))
    values.append(float(sd['Objects count']))
    values.append(float(sd['Lights count']))
    values.append(float(sd['Render Type']))

    return values, [task_id, frames_count]


def select_parameters(cursor):
    function = Function.SIGMOID

    data = []
    tasks = []
    for doc in cursor:
        values, task_dict = select_values(doc, function)
        if values is not None:
            data.append(values)
            tasks.append(task_dict)

    # # with binary props
    normalize_data(data, function, [1, 2, 3, 4])
    # without binary prop
    #normalize_data(data, function, [])

    return data, tasks


def select_all(mongodb):
    return mongodb['3dsmax'].find()


def select_vray(mongodb):
    return mongodb['3dsmax'].find({'renderer': 'VRay'})


def write_data(data, file_path, start, end):
    """ Записывает указанный диапазон (start..end) данных (data) в файл (file).
    :param data: список данных.
    :param file_path: полный путь и имя файла.
    :param start: начальный индекс диапазона.
    :param end: конечный индекс диапазона.
    :return:
    """
    num_values = len(data[0])-1
    num_samples = end - start

    with open(file_path, 'wt') as data_file:
        data_file.write('{samples} {inputs} {outputs}\n'.format(
            samples=num_samples, inputs=num_values, outputs=1
        ))
        for s in range(start, end):
            line = ''
            for i, val in enumerate(data[s][1:]):
                line += '{0:.10f}'.format(val)
                if i < len(data[s][1:])-1:
                    line += ' '
                else:
                    line += '\n'
            data_file.write(line)
            data_file.write('{0:.20f}\n'.format(data[s][0]))


def write_csv(data, csv_file_name):
    myfile = open(csv_file_name, 'wb')
    wr = csv.writer(myfile, delimiter=';')

    for s in data:
        wr.writerow(s)
    myfile.close()


def main():
    train_samples_percent = 98.0
    try:
        conn = MongoClient(host='10.5.0.1', port=27017)
        db = conn.statistic
        #data_cur = select_vray(db)
        data_cur = select_all(db)
        print('Found {} docs with renderer={}'.format(data_cur.count(), 'VRay'))

        data, tasks = select_parameters(data_cur)

        if train_samples_percent > 100.0:
            train_samples_percent = 100.0
        elif train_samples_percent < 0.0:
            train_samples_percent = 0.0

        data_len = len(data)
        train_len = int(ceil(data_len / 100.0 * train_samples_percent))
        test_len = data_len - train_len

        test_data = []

        while len(test_data) < test_len:
            index = random.randint(0, len(data)-1)
            test_data.append(data[index])
            data.remove(data[index])

        write_data(data, 'train.data', 0, len(data)-1)
        write_data(data, 'test.data', 0, len(test_data)-1)

        print('Total: {} samples\nTrain: {}\nTest: {}\n'.format(
              data_len, train_len, test_len))

        write_csv(data, 'data.csv')

        with open('tasks.txt', 'wt') as tasks_file:
            for task in tasks:
                tasks_file.write('{} {}\n'.format(task[0], int(task[1])))

    except Exception as e:
        print("Error: {}".format(e))
        return -1

    return 0


if __name__ == "__main__":
    main()
