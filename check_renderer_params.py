# -*- coding:utf-8 -*-

import os,sys,shutil


def main():
    if len(sys.argv) < 3:
        print("Need params")
    list_path = sys.argv[1]
    script_path = sys.argv[2]

    with open(list_path, 'r') as list_file:
        with open(script_path, 'r') as script_file:
            script_file_text = script_file.read()

            for line in list_file:
                param_name = line[line.index('.')+1:]
                param_name = param_name[:param_name.index(' ')]
                #print(param_name)
                param_type = line[line.index(':')+2:-1]

                if param_name not in script_file_text:
                    if param_type in ['integer', 'float', 'worldUnits']:
                        print("\tformat \"\t \\\"{param_name}\\\":%,\\n\" rc.{param_name}  to:out_file".format(param_name=param_name))
                    else:
                        print("\tformat \"\t \\\"{param_name}\\\":\\\"%\\\",\\n\" rc.{param_name}  to:out_file".format(param_name=param_name))

if __name__ == "__main__":
    main()
